
from selenium import webdriver
import logging
import time

url = 'https://test.pyplan.com/'

driver = webdriver.Chrome()

def setup():
    logging.basicConfig(level=logging.INFO)

def open_page(user, password):
    driver.get(url)
    user_element = driver.find_element_by_name('user')
    user_element.send_keys(user)
    pass_element = driver.find_element_by_name('pass')
    pass_element.send_keys(password)
    butt_element = driver.find_element_by_xpath('//div/button')
    butt_element.click()
    time.sleep(2)
    try:
        loggedin_element = driver.find_element_by_id('currentTask')
        found = True
    except:
        found = False
    return found

def close_page():
    driver.close()

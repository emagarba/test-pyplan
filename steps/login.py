
from behave import given, then, when
from pages import login_page
import logging
import time

login_page.setup()

@given("the user name is '{user}'")
@given('the user name is "{user}"')
def the_user_name_is(context, user):
    context.user = user

@given("the password is '{password}'")
def the_password_is(context, password):
    context.password = password

@given("the password is ''")
def the_password_is_empty(context):
    context.password = ''

@then("the result is '{result}'")
def the_result_is(context, result):
    if result == 'logged':
        logged_in = login_page.open_page(context.user, context.password)
        if not logged_in:
            assert False, 'Not logged in'
    elif result == 'failed':
        logged_in = login_page.open_page(context.user, context.password)
        if logged_in:
            assert False, 'Logged in when it should have failed'
    else:
        logging.error('Unknown result name: "%s"' % result)
        assert False


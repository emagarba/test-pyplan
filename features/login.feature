#
# Testing login to pyplan
#
Feature: Test the login to pyplan

    Tests login for different scenarios

    Scenario Outline: login

        Given the user name is '<user>'
        And   the password is '<password>'
        Then  the result is '<result>'

        Examples: login
            | user        | password  | result     |
            | supertest   | wowowowow | failed     |
            | supertest   | Novix123! | logged     |
            | useruser    | Novix123! | failed     |
